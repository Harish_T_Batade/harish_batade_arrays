function reduce(elements, summation, startingValue) {
    
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    
    let reduced_value=0;
    let initial_index=0;

    if(Array.isArray(elements)==true)
    {
        reduced_value = summation(startingValue,elements)

        return reduced_value;
    }
    else
    {
        return "function only works on Arrays";
    }
}



module.exports = reduce;