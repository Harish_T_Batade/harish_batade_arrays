function find(elements, find_element) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

    let status=false;
    if(Array.isArray(elements)==true && elements.length!=0)
    {
        for (let index = 0; index < elements.length; index++) {        // loop tp call the callback funtion for each element
        
            if(find_element(elements[index]))
            {
                status = find_element(elements[index]);
                break;
            }
            else{
                status = undefined;
            }
        }
    }
    else
    {
        return "function only works on Arrays with atleast one element";
    }

    return status;
}



module.exports = find;