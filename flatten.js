function flatten(elements,depth) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    
    
    return depth > 0 ? elements.reduce((currunt_array, subArray) => currunt_array.concat(Array.isArray(subArray) ? flatten(subArray, depth - 1) : subArray), []): elements.slice();
    };

    module.exports = flatten;