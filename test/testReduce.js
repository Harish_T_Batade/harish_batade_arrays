const items = [1, 2, 2, 4, 5, 5];

const testing_reduce = require('../reduce')

function summation(startingValue,elements)
{
    let answer = 0;
    let initial_index=0;
    if(startingValue === undefined)
        {
            startingValue = elements[0];
            initial_index=1;
        }
            for (let index = initial_index; index < elements.length; index++) {
                    answer += elements[index]
                }
    return answer + startingValue;             
}

const startingValue = 1

console.log(testing_reduce(items,summation,startingValue));

const myResult = testing_reduce(items,summation,startingValue);

console.log(items.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue  }, startingValue))

const originalResult = items.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue  }, startingValue)


    function testReduce(myResult,originalResult) {
    if(myResult!=originalResult)
    {
        return "function reduce failed"
    }

    return "function reduce passed"
}

console.log(testReduce(myResult,originalResult))