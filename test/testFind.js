const items = [1, 2, 3, 4, 5, 5];

const testing_find = require('../find')

/*if find function returns undefined i.e there is no element which passes the test given
 in function "find_element"
 
 given test is to find element greater than 4
 */

function find_element(element)
{
    if(element>4)
    {
        return element;
    }
    else
    {
        return false;
    }
}


const myResult=testing_find(items,find_element);

const originalResult=items.find(function find_element(element)
{
    if(element>4)
    {
        console.log("element : "+element )
        return element;
    }
    else
    {
        
        return false;
    }
})

console.log(myResult)
